# rrterm - Rust Roguelike Terminal
A terminal imposter for roguelike games in Rust.

- Colored and alpha blended tilesets/fonts.
- Multiple layers of tiles.
- GPU accelerated rendering.
- Provides a ggez context for filesystem & audio utilities.

![Kitchen Sink Example](kitchen_sink.png)

## Usage
rrterm is a crate for the [Rust Programming Language](https://www.rust-lang.org/).
You need `rustc` and `cargo` to use it, which are included with a default Rust
installation.

rrterm is built on ggez, which in turn is built on SDL2. Because of this you
have to set up SDL2 for use with Rust.
[You can find a guide on how to do that here](https://github.com/Rust-SDL2/rust-sdl2#user-content-requirements).

On Windows, I suggest you follow either the "Windows (MinGW)" or "Windows (MSVC)"
instructions, rather than "Windows with build script", as these are much simpler.
If you aren't sure if you need MinGW or MSVC, by default Rust will be installed
with the MinGW toolchain.

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
