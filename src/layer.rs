use {
    std::{
        rc::{Rc},
    },

    cgmath::{Point2, Vector2},
    ggez::{GameResult},
    palette::rgb::{LinSrgba},

    terminal::{Vertex},
    Terminal, Tileset,
};

const MESH_STRIDE: usize = 4;

/// A layer that can be displayed on a terminal.
pub struct Layer {
    pub(crate) tileset: Rc<Tileset>,

    grid_size: Vector2<i32>,
    pub(crate) vertices: Vec<Vertex>,
    pub(crate) indices: Vec<u16>,
}

impl Layer {
    /// Creates a new layer.
    pub fn new(
        terminal: &mut Terminal, tileset: Rc<Tileset>, grid_size: Vector2<i32>,
    ) -> GameResult<Self> {
        let tile_size = terminal.tile_size();

        let (_, uv) = tileset.uvs(0);

        let amount = (grid_size.x * grid_size.y) as usize;
        let mut vertices = Vec::with_capacity(4 * amount);
        let mut indices = Vec::with_capacity(6 * amount);
        for y in 0..grid_size.y {
            for x in 0..grid_size.x {
                let position_s =
                    Point2::new(tile_size.x * x, tile_size.y * y).cast().unwrap();
                let position_e =
                    Point2::new(tile_size.x * (x+1), tile_size.y * (y+1)).cast().unwrap();

                let index_s = vertices.len() as u16;
                vertices.push(Vertex {
                    pos: [position_s.x, position_s.y],
                    tex_coord: [0.0, 0.0],
                    foreground: [1.0, 1.0, 1.0, 1.0],
                    background: [0.0, 0.0, 0.0, 0.0],
                });
                vertices.push(Vertex {
                    pos: [position_e.x, position_s.y],
                    tex_coord: [uv.x, 0.0],
                    foreground: [1.0, 1.0, 1.0, 1.0],
                    background: [0.0, 0.0, 0.0, 0.0],
                });
                vertices.push(Vertex {
                    pos: [position_s.x, position_e.y],
                    tex_coord: [0.0, uv.y],
                    foreground: [1.0, 1.0, 1.0, 1.0],
                    background: [0.0, 0.0, 0.0, 0.0],
                });
                vertices.push(Vertex {
                    pos: [position_e.x, position_e.y],
                    tex_coord: [uv.x, uv.y],
                    foreground: [1.0, 1.0, 1.0, 1.0],
                    background: [0.0, 0.0, 0.0, 0.0],
                });

                indices.push(index_s + 0);
                indices.push(index_s + 3);
                indices.push(index_s + 1);

                indices.push(index_s + 0);
                indices.push(index_s + 2);
                indices.push(index_s + 3);
            }
        }

        Ok(Layer {
            tileset,

            grid_size,
            vertices,
            indices,
        })
    }

    /// Overwrites all data of a tile in the grid.
    pub fn put(
        &mut self, position: Point2<i32>, tile: u32, foreground: LinSrgba, background: LinSrgba,
    ) {
        if !self.in_range(position) {
            return
        }

        let index = self.index_for_position(position);

        for vertex in &mut self.vertices[index..index+MESH_STRIDE] {
            vertex.foreground = [
                foreground.color.red,
                foreground.color.green,
                foreground.color.blue,
                foreground.alpha,
            ];
            vertex.background = [
                background.color.red,
                background.color.green,
                background.color.blue,
                background.alpha,
            ];
        }

        self.set_uvs(tile, index);
    }

    /// Overwrites just the tile in the grid.
    pub fn put_tile(&mut self, position: Point2<i32>, tile: u32) {
        if !self.in_range(position) {
            return
        }

        let index = self.index_for_position(position);
        self.set_uvs(tile, index);
    }

    /// Prints a string starting at a position in the grid.
    pub fn print(&mut self, position: Point2<i32>, text: &str) {
        if !self.in_range(position) { return }

        for (i, character) in text.chars().enumerate() {
            self.put_tile(position + Vector2::new(i as i32, 0), character as u32);
        }
    }

    fn index_for_position(&self, position: Point2<i32>) -> usize {
         (position.x + position.y * self.grid_size.x) as usize * MESH_STRIDE
    }

    fn in_range(&self, position: Point2<i32>) -> bool {
        position.x >= 0 && position.y >= 0 &&
        position.x < self.grid_size.x && position.y < self.grid_size.y
    }

    fn set_uvs(&mut self, tile: u32, start_index: usize) {
        let (uv_s, uv_e) = self.tileset.uvs(tile);

        self.vertices[start_index + 0].tex_coord = [uv_s.x, uv_s.y];
        self.vertices[start_index + 1].tex_coord = [uv_e.x, uv_s.y];
        self.vertices[start_index + 2].tex_coord = [uv_s.x, uv_e.y];
        self.vertices[start_index + 3].tex_coord = [uv_e.x, uv_e.y];
    }

    /// Clears the grid back to default values.
    pub fn clear(&mut self) {
        for y in 0..self.grid_size.y {
            for x in 0..self.grid_size.x {
                self.put(
                    Point2::new(x, y), 0,
                    (1.0, 1.0, 1.0, 1.0).into(),
                    (0.0, 0.0, 0.0, 0.0).into(),
                );
            }
        }
    }
}
