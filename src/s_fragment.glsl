#version 150 core

uniform sampler2D u_texture;

in vec2 v_tex_coord;
in vec4 v_foreground;
in vec4 v_background;

out vec4 o_color;

void main() {
    vec4 tex = texture(u_texture, v_tex_coord);
    vec4 tex_foreground = tex * v_foreground;

    o_color = mix(v_background, tex_foreground, tex.a);
}
