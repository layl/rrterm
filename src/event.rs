//! Terminal events and event data.

// Convenience re-exports, so the library user doesn't have to pull in all dependencies
pub use {
    ggez::event::{Keycode},
};

/// A terminal event.
pub enum Event {
    /// The close button on the window has been pressed.
    Close,
    /// A key has been pressed, released, or repeated.
    Key { keycode: Keycode, pressed: bool, repeat: bool },
    /// Text input occurred, may be the result of a keypress.
    Text { text: String, },
}
