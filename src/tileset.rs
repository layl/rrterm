use {
    std::{
        io::{Read},
        rc::{Rc},
    },

    cgmath::{Vector2, Point2},
    ggez::{
        graphics::{self},
        GameResult,
    },
    gfx::{
        texture::{SamplerInfo, Kind, Mipmap, AaMode, FilterMethod, WrapMode},
        handle::{ShaderResourceView, Sampler},
        self, Factory,
    },
    image,

    Resources, Terminal,
};

/// A set of tiles to be put in a layer.
pub struct Tileset {
    pub(crate) view: ShaderResourceView<Resources, [f32; 4]>,
    pub(crate) sampler: Sampler<Resources>,

    size_in_tiles: Vector2<i32>,
    uv_per_tile: Vector2<f32>,
    direction: Direction,
}

impl Tileset {
    /// Loads a tileset from file.
    pub fn load(
        terminal: &mut Terminal, path: &str, direction: Direction,
    ) -> GameResult<Rc<Self>> {
        let tile_size = terminal.tile_size();

        let mut buffer = Vec::new();
        let mut reader = terminal.ctx().filesystem.open(path)?;
        reader.read_to_end(&mut buffer).unwrap();

        let (factory, _device, _encoder, _depth_view, _color_view) =
            graphics::get_gfx_objects(terminal.ctx());

        let image = image::load_from_memory(&buffer).unwrap().to_rgba();
        let image_dimensions = image.dimensions();

        let data: [&[u8]; 1] = [&image.into_raw()];
        let (_, view) = factory
            .create_texture_immutable_u8::<gfx::format::Srgba8>(
                Kind::D2(image_dimensions.0 as u16, image_dimensions.1 as u16, AaMode::Single),
                Mipmap::Provided,
                &data,
            )
            .unwrap();

        let sinfo = SamplerInfo::new(FilterMethod::Bilinear, WrapMode::Clamp);
        let sampler = factory.create_sampler(sinfo);

        let size_in_tiles = Vector2::new(
            image_dimensions.0 as i32 / tile_size.x,
            image_dimensions.1 as i32 / tile_size.y,
        );
        let uv_per_tile = Vector2::new(1.0 / size_in_tiles.x as f32, 1.0 / size_in_tiles.y as f32);

        Ok(Rc::new(Tileset {
            view,
            sampler,

            size_in_tiles,
            uv_per_tile,
            direction,
        }))
    }

    pub fn uvs(&self, tile: u32) -> (Point2<f32>, Point2<f32>) {
        let tile = match self.direction {
            Direction::Horizontal => Point2::new(
                tile as i32 % self.size_in_tiles.x,
                tile as i32 / self.size_in_tiles.x,
            ),
            Direction::Vertical => Point2::new(
                tile as i32 / self.size_in_tiles.y,
                tile as i32 % self.size_in_tiles.y,
            ),
        };

        let uv_s = Point2::new(
            tile.x as f32 * self.uv_per_tile.x,
            tile.y as f32 * self.uv_per_tile.y,
        );
        let uv_e = uv_s + self.uv_per_tile;

        (uv_s, uv_e)
    }
}

#[derive(Clone, Copy)]
pub enum Direction {
    Horizontal,
    Vertical,
}
