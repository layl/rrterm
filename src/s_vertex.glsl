#version 150 core

uniform Locals {
    mat4 u_transform;
};

in vec2 a_pos;
in vec2 a_tex_coord;
in vec4 a_foreground;
in vec4 a_background;

out vec2 v_tex_coord;
out vec4 v_foreground;
out vec4 v_background;

void main() {
    v_tex_coord = a_tex_coord;
    v_foreground = a_foreground;
    v_background = a_background;

    gl_Position = u_transform * vec4(a_pos, 0.0, 1.0);
    gl_ClipDistance[0] = 1.0;
}
