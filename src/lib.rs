extern crate cgmath;
extern crate ggez;
#[macro_use] extern crate gfx;
extern crate image;
extern crate palette;

pub mod event;
mod layer;
mod terminal;
mod tileset;

// Convenience re-exports, so the library user doesn't have to pull in all dependencies
pub use {
    cgmath::{Vector2, Point2, EuclideanSpace},
    ggez::{GameResult},
    palette::{Srgba, LinSrgba},
};

pub use self::{
    layer::{Layer},
    terminal::{Terminal},
    tileset::{Tileset, Direction},
};

type Resources = <ggez::graphics::GlBackendSpec as ggez::graphics::BackendSpec>::Resources;
