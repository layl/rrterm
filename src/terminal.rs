use {
    std::{
        path::{PathBuf},
    },

    cgmath::{Vector2, Point2, Vector3, Ortho, Matrix4},
    ggez::{
        event::{Events, Event as GEvent},
        conf::{Conf, WindowMode, WindowSetup},
        graphics::{self},
        Context, GameResult,
    },
    gfx::{
        traits::{FactoryExt},
        handle::{Buffer},
        memory::{Typed},
        preset::{blend, depth},
        state::{ColorMask},
        self, PipelineState, VertexBuffer, ConstantBuffer, TextureSampler, BlendTarget,
        DepthTarget,
    },

    event::{Event},
    Resources, Layer,
};

type ColorFormat = gfx::format::Srgba8;
type DepthFormat = gfx::format::DepthStencil;

gfx_defines!{
    vertex Vertex {
        pos: [f32; 2] = "a_pos",
        tex_coord: [f32; 2] = "a_tex_coord",
        foreground: [f32; 4] = "a_foreground",
        background: [f32; 4] = "a_background",
    }

    constant Locals {
        transform: [[f32; 4]; 4] = "u_transform",
    }

    pipeline pipe {
        vbuf: VertexBuffer<Vertex> = (),
        locals: ConstantBuffer<Locals> = "Locals",
        texture: TextureSampler<[f32; 4]> = "u_texture",
        out_color: BlendTarget<ColorFormat> = ("o_color", ColorMask::all(), blend::ALPHA),
        out_depth: DepthTarget<DepthFormat> = depth::LESS_EQUAL_WRITE,
    }
}

/// A terminal emulator instance.
pub struct Terminal {
    ctx: Context,
    events: Events,
    fullscreen: bool,

    pso: PipelineState<Resources, pipe::Meta>,
    locals: Buffer<Resources, Locals>,

    tile_size: Vector2<i32>,
    grid_size: Vector2<i32>,
}

impl Terminal {
    /// Creates a new terminal.
    pub fn new<S>(
        game_id: &'static str, author: &'static str, title: S,
        tile_size: Vector2<i32>, grid_size: Vector2<i32>,
    ) -> GameResult<Self> where
        S: Into<String>
    {
        // Set up the ggez context
        let mut c = Conf::new();
        c.window_mode = WindowMode {
            width: (tile_size.x * grid_size.x) as u32,
            height: (tile_size.y * grid_size.y) as u32,
            .. Default::default()
        };
        c.window_setup = WindowSetup {
            title: title.into(),
            .. Default::default()
        };
        let mut ctx = Context::load_from_conf(game_id, author, c)?;
        let events = Events::new(&mut ctx)?;

        // Add the resources directory relative to the current working path, which should be the
        // root folder of the crate, or the distribution folder
        let path = PathBuf::from("./resources");
        ctx.filesystem.mount(&path, true);

        let (pso, locals) = {
            let (factory, _device, _encoder, _depth_view, _color_view) =
                graphics::get_gfx_objects(&mut ctx);

            // Create pipeline state object
            let vs = include_bytes!("s_vertex.glsl");
            let fs = include_bytes!("s_fragment.glsl");
            let set = factory.create_shader_set(vs, fs).unwrap();
            let pso = factory.create_pipeline_state(
                &set,
                gfx::Primitive::TriangleList,
                gfx::state::Rasterizer::new_fill().with_cull_back(),
                pipe::new()
            ).unwrap();

            let locals = factory.create_constant_buffer(1);

            (pso, locals)
        };

        Ok(Terminal {
            ctx,
            events,
            fullscreen: false,

            pso,
            locals,

            tile_size,
            grid_size,
        })
    }

    /// Get the inner ggez Context.
    pub fn ctx(&mut self) -> &mut Context {
        &mut self.ctx
    }

    /// Gets the size of individual tiles in pixels.
    pub fn tile_size(&self) -> Vector2<i32> {
        self.tile_size
    }

    /// Gets the grid size in tiles.
    pub fn grid_size(&self) -> Vector2<i32> {
        self.grid_size
    }

    pub fn fullscreen(&self) -> bool {
        self.fullscreen
    }

    pub fn set_fullscreen(&mut self, value: bool) -> GameResult<()> {
        graphics::set_fullscreen(&mut self.ctx, value)?;
        self.fullscreen = value;
        Ok(())
    }

    /// Polls for events.
    pub fn poll_event(&mut self) -> Option<Event> {
        while let Some(event) = self.events.poll().next() {
            match event {
                GEvent::Quit { .. } =>
                    return Some(Event::Close),
                GEvent::KeyDown { keycode: Some(keycode), repeat, .. } =>
                    return Some(Event::Key { keycode, pressed: true, repeat }),
                GEvent::KeyUp { keycode: Some(keycode), repeat, .. } =>
                    return Some(Event::Key { keycode, pressed: false, repeat }),
                GEvent::TextInput { text, .. } =>
                    return Some(Event::Text { text }),
                _ => {}
            }
        }

        None
    }

    /// Presents the layers on the terminal, drawing them in sequence.
    pub fn present(&mut self, layers: &[(&Layer, Point2<i32>)]) {
        let (window_width, window_height) = graphics::get_size(&mut self.ctx);

        {
            let (factory, device, encoder, depth_view, color_view) =
                graphics::get_gfx_objects(&mut self.ctx);

            let color_view = Typed::new(color_view);
            let depth_view = Typed::new(depth_view);

            encoder.clear(&color_view, [0.0, 0.0, 0.0, 1.0]);
            encoder.clear_depth(&depth_view, 1.0);

            let projection: Matrix4<f32> = Ortho {
                left: 0.0,
                right: window_width as f32,
                bottom: window_height as f32,
                top: 0.0,
                near: -1.0,
                far: 1.0,
            }.into();

            for (layer, position) in layers {
                let (vbuf, slice) = factory.create_vertex_buffer_with_slice(
                    layer.vertices.as_slice(), layer.indices.as_slice()
                );

                let position = Vector3::new(
                    (position.x * self.tile_size.x) as f32,
                    (position.y * self.tile_size.y) as f32,
                    0.0,
                );
                let model = Matrix4::from_translation(position);
                let locals = Locals {
                    transform: (projection * model).into(),
                };
                encoder.update_constant_buffer(&self.locals, &locals);

                let data = pipe::Data {
                    vbuf: vbuf,
                    locals: self.locals.clone(),
                    texture: (layer.tileset.view.clone(), layer.tileset.sampler.clone()),
                    out_color: color_view.clone(),
                    out_depth: depth_view.clone(),
                };
                encoder.draw(&slice, &self.pso, &data);
            }

            encoder.flush(device);
        }

        graphics::present(&mut self.ctx);
    }
}
