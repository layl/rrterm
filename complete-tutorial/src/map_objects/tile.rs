#[derive(Clone)]
pub struct Tile {
    pub block: bool,
    pub block_sight: bool,
}
