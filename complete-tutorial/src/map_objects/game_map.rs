use {
    rrterm::{Vector2},

    map_objects::{Tile},
};

pub struct GameMap {
    tiles: Vec<Tile>,
    size: Vector2<i32>,
}

impl GameMap {
    pub fn new(size: Vector2<i32>) -> GameMap {
        GameMap {
            tiles: initialize_tiles(size),
            size,
        }
    }
}

fn initialize_tiles(size: Vector2<i32>) -> Vec<Tile> {
    let mut tiles = vec![Tile { block: false, block_sight: false }; (size.x * size.y) as usize];

    let y = (22 * size.x) as usize;
    tiles[30 + y].block = true;
    tiles[30 + y].block_sight = true;
    tiles[31 + y].block = true;
    tiles[31 + y].block_sight = true;
    tiles[32 + y].block = true;
    tiles[32 + y].block_sight = true;

    tiles
}
