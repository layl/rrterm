extern crate rrterm;

mod map_objects;
mod entity;
mod input_handlers;
mod render_functions;

use {
    std::error::{Error},

    rrterm::{
        event::{Event},
        Vector2, Point2, Terminal, Layer, Tileset, Srgba, Direction, EuclideanSpace,
    },

    entity::{Entity},
    input_handlers::{handle_key, Action},
    render_functions::{render_all, clear_all},
};

fn main() -> Result<(), Box<Error>> {
    let screen_size = Vector2::new(80, 50);

    let mut terminal = Terminal::new(
        "complete_tutorial", "rrterm", "rrterm does complete roguelike tutorial",
        Vector2::new(8, 8), screen_size,
    )?;
    let tileset = Tileset::load(&mut terminal, "/terminal.png", Direction::Vertical)?;
    let mut layer = Layer::new(&mut terminal, tileset, screen_size)?;

    let player = Entity {
        position: Point2::from_vec(screen_size / 2),
        tile: 1,
        color: Srgba::new(1.0, 1.0, 1.0, 1.0).into_linear()
    };
    let npc = Entity {
        position: Point2::from_vec(screen_size / 2) + Vector2::new(-5, 0),
        tile: 1,
        color: Srgba::new(1.0, 1.0, 0.0, 1.0).into_linear(),
    };
    let mut entities = vec!(player, npc);

    'game: loop {
        while let Some(event) = terminal.poll_event() {
            match event {
                Event::Close => break 'game,
                Event::Key { keycode, pressed: true, .. } => {
                    match handle_key(keycode) {
                        Action::Quit => break 'game,
                        Action::Move(motion) => entities[0].apply_move(motion),
                        Action::ToggleFullscreen => {
                            let fullscreen = terminal.fullscreen();
                            terminal.set_fullscreen(!fullscreen)?;
                        },
                        Action::None => {},
                    }
                },
                _ => {},
            }
        }

        render_all(&mut terminal, &mut layer, &entities);
        clear_all(&mut layer, &entities);
    }

    Ok(())
}
