use {
    rrterm::{
        Point2, Terminal, Layer, Srgba,
    },

    entity::{Entity},
};

pub fn render_all(terminal: &mut Terminal, layer: &mut Layer, entities: &Vec<Entity>) {
    for entity in entities {
        draw_entity(layer, entity)
    }

    terminal.present(&[(&layer, Point2::new(0, 0))]);
}

fn draw_entity(layer: &mut Layer, entity: &Entity) {
    let background = Srgba::new(0.0, 0.0, 0.0, 0.0).into_linear();

    layer.put(entity.position, entity.tile, entity.color, background);
}

pub fn clear_all(layer: &mut Layer, entities: &Vec<Entity>) {
    for entity in entities {
        clear_entity(layer, entity);
    }
}

fn clear_entity(layer: &mut Layer, entity: &Entity) {
    layer.put_tile(entity.position, 0);
}
