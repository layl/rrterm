use {
    rrterm::{event::{Keycode}, Vector2},
};

pub fn handle_key(keycode: Keycode) -> Action {
    match keycode {
        Keycode::Escape => Action::Quit,
        Keycode::F1 => Action::ToggleFullscreen,
        Keycode::Up => Action::Move(Vector2::new(0, -1)),
        Keycode::Down => Action::Move(Vector2::new(0, 1)),
        Keycode::Left => Action::Move(Vector2::new(-1, 0)),
        Keycode::Right => Action::Move(Vector2::new(1, 0)),
        _ => Action::None,
    }
}

pub enum Action {
    Quit,
    Move(Vector2<i32>),
    ToggleFullscreen,
    None,
}
