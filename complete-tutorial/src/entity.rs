use {
    rrterm::{
        Vector2, Point2, LinSrgba,
    },
};

pub struct Entity {
    pub position: Point2<i32>,
    pub tile: u32,
    pub color: LinSrgba,
}

impl Entity {
    pub fn apply_move(&mut self, distance: Vector2<i32>) {
        self.position += distance;
    }
}
