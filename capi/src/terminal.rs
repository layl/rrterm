extern crate rrterm;

use {
    std::{
        ffi::{CStr},
        os::raw::{c_char},
        slice,
    },

    rrterm::{event::{Event}, Terminal, Vector2, Point2},

    layer::{RRLayer},
    RREvent,
};

pub struct RRTerminal(pub Terminal);

#[no_mangle]
pub extern fn terminal_new(
    game: *const c_char, author: *const c_char, window_title: *const c_char,
    tile_width: i32, tile_height: i32, grid_width: i32, grid_height: i32,
) -> *mut RRTerminal {
    let game = unsafe { CStr::from_ptr(game) };
    let author = unsafe { CStr::from_ptr(author) };
    let window_title = unsafe { CStr::from_ptr(window_title) };

    let terminal = Terminal::new(
        game.to_str().unwrap(), author.to_str().unwrap(), window_title.to_str().unwrap(),
        Vector2::new(tile_width, tile_height), Vector2::new(grid_width, grid_height),
    ).unwrap();

    Box::into_raw(Box::new(RRTerminal(terminal)))
}

#[no_mangle]
pub extern fn terminal_drop(terminal: *mut RRTerminal) {
    unsafe { Box::from_raw(terminal); }
}

#[no_mangle]
pub extern fn terminal_poll_event(terminal: *mut RRTerminal) -> RREvent {
    let terminal = unsafe { &mut (*terminal).0 };

    match terminal.poll_event() {
        Some(event) => match event {
            Event::Close => RREvent::Close,
            Event::Key { .. } => RREvent::Key,
            Event::Text { .. } => RREvent::Text,
        },
        None => RREvent::None,
    }
}

#[no_mangle]
pub extern fn terminal_present(
    terminal: *mut RRTerminal, layers: *const *const RRLayer, layers_len: usize,
) {
    let terminal = unsafe { &mut (*terminal).0 };
    let layers = unsafe { slice::from_raw_parts(layers, layers_len as usize) };

    let layers: Vec<_> = layers.iter()
        .map(|layer| (unsafe { &(**layer).0 }, Point2::new(0, 0)))
        .collect();

    terminal.present(&layers);
}
