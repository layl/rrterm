use {
    rrterm::{Layer, Point2, Vector2},

    terminal::{RRTerminal},
    tileset::{RRTileset},
    RRColor,
};

pub struct RRLayer(pub Layer);

#[no_mangle]
pub extern fn layer_new(
    terminal: *mut RRTerminal, tileset: *mut RRTileset, grid_width: i32, grid_height: i32
) -> *mut RRLayer {
    let terminal = unsafe { &mut (*terminal).0 };
    let tileset = unsafe { &mut (*tileset).0 };

    let layer = Layer::new(
        terminal, tileset.clone(), Vector2::new(grid_width, grid_height),
    ).unwrap();

    Box::into_raw(Box::new(RRLayer(layer)))
}

#[no_mangle]
pub extern fn layer_drop(layer: *mut RRLayer) {
    unsafe { Box::from_raw(layer); }
}

#[no_mangle]
pub extern fn layer_put(
    layer: *mut RRLayer, x: i32, y: i32, tile: u32, foreground: RRColor, background: RRColor,
) {
    let layer = unsafe { &mut (*layer).0 };

    let foreground = (foreground.r, foreground.g, foreground.b, foreground.a).into();
    let background = (background.r, background.g, background.b, background.a).into();

    layer.put(Point2::new(x, y), tile, foreground, background);
}
