use {
    std::{
        ffi::{CStr},
        os::raw::{c_char},
        rc::{Rc},
    },
    rrterm::{Tileset, Direction},

    terminal::{RRTerminal},
};

pub struct RRTileset(pub Rc<Tileset>);

#[no_mangle]
pub extern fn tileset_load(
    terminal: *mut RRTerminal, path: *const c_char, direction: RRDirection,
) -> *mut RRTileset {
    let terminal = unsafe { &mut (*terminal).0 };
    let path = unsafe { CStr::from_ptr(path) };
    let direction = match direction {
        RRDirection::Horizontal => Direction::Horizontal,
        RRDirection::Vertical => Direction::Vertical,
    };

    let tileset = Tileset::load(terminal, path.to_str().unwrap(), direction).unwrap();

    Box::into_raw(Box::new(RRTileset(tileset)))
}

#[no_mangle]
pub extern fn tileset_drop(tileset: *mut RRTileset) {
    unsafe { Box::from_raw(tileset); }
}

#[repr(C)]
pub enum RRDirection {
    Horizontal,
    Vertical,
}
