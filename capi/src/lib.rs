extern crate rrterm;

pub mod layer;
pub mod terminal;
pub mod tileset;

use rrterm::{Srgba};

#[repr(C)]
pub struct RRColor { r: f32, g: f32, b: f32, a: f32 }

#[no_mangle]
pub extern fn color_from_srgba(r: u8, g: u8, b: u8, a: u8) -> RRColor {
    let color: Srgba = (
        r as f32 / 255.0,
        g as f32 / 255.0,
        b as f32 / 255.0,
        a as f32 / 255.0,
    ).into();
    let color = color.into_linear();

    RRColor { r: color.red, g: color.green, b: color.blue, a: color.alpha }
}

#[repr(C)]
pub enum RREvent {
    None,
    Close,
    Key,
    Text,
}
