#include "rrterm.h"

int main()
{
	RRTerminal *terminal = terminal_new("rrterm-cgame", "rrterm", "rrterm c game", 8, 8, 50, 50);
	RRTileset *tileset = tileset_load(terminal, "/terminal.png");
	RRLayer *layer = layer_new(terminal, tileset);

	RRColor foreground = color_from_srgba(100, 255, 100, 255);
	RRColor background = color_from_srgba(0, 0, 0, 0);

	while (true) {
		if (terminal_poll_event(terminal) == RREvent::Close) {
			break;
		}

		layer_put(layer, 5, 5, 1, foreground, background);

		RRLayer *layers[1] = { layer };
		terminal_present(terminal, layers, 1);
	}

	layer_drop(layer);
	tileset_drop(tileset);
	terminal_drop(terminal);

    return 0;
}
