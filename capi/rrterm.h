#include <cstdint>
#include <cstdlib>

enum class RRDirection {
  Horizontal,
  Vertical,
};

enum class RREvent {
  None,
  Close,
  Key,
  Text,
};

struct RRLayer;

struct RRTerminal;

struct RRTileset;

struct RRColor {
  float r;
  float g;
  float b;
  float a;
};

extern "C" {

RRColor color_from_srgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

void layer_drop(RRLayer *layer);

RRLayer *layer_new(RRTerminal *terminal,
                   RRTileset *tileset,
                   int32_t grid_width,
                   int32_t grid_height);

void layer_put(RRLayer *layer,
               int32_t x,
               int32_t y,
               uint32_t tile,
               RRColor foreground,
               RRColor background);

void terminal_drop(RRTerminal *terminal);

RRTerminal *terminal_new(const char *game,
                         const char *author,
                         const char *window_title,
                         int32_t tile_width,
                         int32_t tile_height,
                         int32_t grid_width,
                         int32_t grid_height);

RREvent terminal_poll_event(RRTerminal *terminal);

void terminal_present(RRTerminal *terminal, const RRLayer *const *layers, uintptr_t layers_len);

void tileset_drop(RRTileset *tileset);

RRTileset *tileset_load(RRTerminal *terminal, const char *path, RRDirection direction);

} // extern "C"
