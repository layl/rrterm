extern crate palette;
extern crate rrterm;

use {
    std::error::{Error},

    palette::{LinSrgba, Hsv, Gradient},
    rrterm::{event::{Event}, Vector2, Point2, Terminal, Layer, Tileset, Direction},
};

fn main() -> Result<(), Box<Error>> {
    let size = Vector2::new(50, 50);

    let mut terminal = Terminal::new(
        "kitchen_sink", "rrterm", "Kitchen Sink",
        Vector2::new(8, 8), size,
    )?;
    let tileset = Tileset::load(&mut terminal, "/terminal.png", Direction::Vertical)?;
    let mut layer = Layer::new(&mut terminal, tileset.clone(), size)?;
    let mut top_layer = Layer::new(&mut terminal, tileset, size)?;

    'game: loop {
        while let Some(event) = terminal.poll_event() {
            match event {
                Event::Close => break 'game,
                _ => {},
            }
        }

        layer.clear();

        // Simple text output
        layer.print(Point2::new(1, 1), "Kitchen Sink Example!");

        // Colored individual tiles
        layer.print(Point2::new(1, 3), "Colored Individual Tiles");
        let gradient = Gradient::new(vec![
            Hsv::from(LinSrgba::new(1.0, 0.1, 0.1, 1.0)),
            Hsv::from(LinSrgba::new(0.1, 1.0, 1.0, 1.0))
        ]);
        for (i, color) in gradient.take(21).enumerate() {
            layer.put(
                Point2::new(i as i32 + 1, 5), i as u32 + 1,
                LinSrgba::new(0.0, 0.0, 0.0, 1.0), color.into(),
            );
        }

        // Multiple Layers
        layer.print(Point2::new(1, 7), "Multiple Layers");
        for y in 0..5 {
            for x in 0..21 {
                layer.put(
                    Point2::new(x + 1, y + 9), 176,
                    LinSrgba::new(0.3, 0.3, 0.3, 1.0),
                    LinSrgba::new(0.2, 0.2, 0.2, 1.0),
                );
            }
        }
        let alpha_color = LinSrgba::new(0.0, 0.0, 0.0, 0.0);
        let unit_color = LinSrgba::new(1.0, 1.0, 1.0, 1.0);
        top_layer.put(Point2::new(5, 11), 'A' as u32, unit_color, alpha_color);
        top_layer.put(Point2::new(10, 12), 1, unit_color, alpha_color);
        top_layer.put(Point2::new(19, 10), 6, unit_color, alpha_color);

        terminal.present(&[(&layer, Point2::new(0, 0)), (&top_layer, Point2::new(0, 0))]);
    }

    Ok(())
}
