extern crate rrterm;

use {
    std::error::{Error},

    rrterm::{event::{Event}, Vector2, Point2, Terminal, Layer, Tileset, Direction},
};

fn main() -> Result<(), Box<Error>> {
    let size = Vector2::new(50, 50);

    let mut terminal = Terminal::new(
        "hello_world", "rrterm", "Hello World",
        Vector2::new(8, 8), size,
    )?;
    let tileset = Tileset::load(&mut terminal, "/terminal.png", Direction::Vertical)?;
    let mut layer = Layer::new(&mut terminal, tileset, size)?;

    'game: loop {
        while let Some(event) = terminal.poll_event() {
            match event {
                Event::Close => break 'game,
                _ => {},
            }
        }

        layer.clear();

        layer.print(Point2::new(1, 1), "Hello World!");

        terminal.present(&[(&layer, Point2::new(0, 0))]);
    }

    Ok(())
}
